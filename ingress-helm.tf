resource "helm_release" "nginx_ingress" {
  timeout = 600
  depends_on = [linode_lke_cluster.foobar]
  name       = "nginx-ingress-controller"

  #repository = "https://charts.bitnami.com/bitnami"
  repository =  "https://helm.nginx.com/stable"
  #chart      = "nginx-ingress-controller"
  chart = "nginx-ingress"
  namespace = "ingress-nginx"
  create_namespace = true
  wait = true

  set {
    name  = "service.type"
    value = "LoadBalancer"
  }

}