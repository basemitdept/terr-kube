data "kubectl_path_documents" "other-name" {
  pattern = "./k8s/*.yaml"
}

resource "kubectl_manifest" "some-name" {
  depends_on = [linode_lke_cluster.foobar]
  for_each  = toset(data.kubectl_path_documents.other-name.documents)
  yaml_body = each.value
}