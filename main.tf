terraform {
  required_providers {
    linode = {
      source  = "linode/linode"
      version = "2.21.2"
    }
    helm = {
       source = "hashicorp/helm"
     
    }

    kubectl = {
      source  = "gavinbunney/kubectl"
      version = ">= 1.7.0"
    }
    kubernetes = {
      source  = "hashicorp/kubernetes"
    }
  }
}

provider "linode" {
  token = ""
}



locals {
  kubeconfig = base64decode(linode_lke_cluster.foobar.kubeconfig)  # Decode the base64 encoded kubeconfig file
  kubeconfig_data = yamldecode(local.kubeconfig)  # Parse the decoded YAML data
}



resource "local_file" "kubeconfig" {
  depends_on = [linode_lke_cluster.foobar]
  filename   = "kube-config"
  content    = base64decode(local.kubeconfig_data.clusters[0].cluster.certificate-authority-data)
}


provider "kubernetes" {
   host = local.kubeconfig_data.clusters[0].cluster.server
   cluster_ca_certificate = base64decode(local.kubeconfig_data.clusters[0].cluster.certificate-authority-data)
   token = local.kubeconfig_data.users[0].user.token
}


provider "helm" {
  kubernetes {
    host = local.kubeconfig_data.clusters[0].cluster.server
    cluster_ca_certificate = base64decode(local.kubeconfig_data.clusters[0].cluster.certificate-authority-data)
    token = local.kubeconfig_data.users[0].user.token
  }
}

provider "kubectl" {
   host = local.kubeconfig_data.clusters[0].cluster.server
   cluster_ca_certificate = base64decode(local.kubeconfig_data.clusters[0].cluster.certificate-authority-data)
   token = local.kubeconfig_data.users[0].user.token
   load_config_file       = false
}

