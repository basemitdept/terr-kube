resource "linode_domain_record" "ingress_dns" {
 

  domain_id = "3007542"
  record_type      = "A"
  name      = "ingress"  # Subdomain name
  target    = data.kubernetes_service.ingress_nginx.status.0.load_balancer.0.ingress.0.ip
  ttl_sec   = 300
  
}