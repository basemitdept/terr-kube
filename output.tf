
data "kubernetes_service" "ingress_nginx" {
  metadata {
    name = "nginx-ingress-controller-controller"
    namespace = "ingress-nginx"
  }
  depends_on = [
    helm_release.nginx_ingress
  ]
}





output "k8s_service_ingress" {
  description   = "External DN name of load balancer"
  value         = data.kubernetes_service.ingress_nginx.status.0.load_balancer.0.ingress.0.ip    #to use this to add dns 
}


